import os

file_name = "data/heat_engine_data"
file_name_new = "data/heat_engine_data.csv"  

replaced: str

with open(file_name, "r") as f:
    replaced = f.read().replace(" ", ",")
    replaced = replaced.replace("\t", ",")

with open(file_name_new, "w") as f:
    f.write(replaced)